FROM 192.168.0.83:20202/hwcse/as-go:1.8.5

COPY ./sdzoserver /home
COPY ./conf /home/conf
RUN chmod +x /home/sdzoserver

CMD ["/home/sdzgoserver"]